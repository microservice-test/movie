const express = require("express");
const { check, validationResult } = require("express-validator");
const Movies = require("./models/movie_model");
const app = express();
var mongoose = require("mongoose");
const bodyParser = require("body-parser");

app.use(bodyParser.json());

const validatorPost = [
  check("movie_name").not().isEmpty().trim().escape(),
  check("release_date").not().isEmpty().trim().escape(),
  check("duration").not().isEmpty().trim().escape(),
  (req, res, next) => {
    const errors = validationResult(req).array();
    if (errors.length) {
      return res.status(422).json({ errors });
    }
    next();
  },
];

//API to get the movies list
app.get("/api/v1/movies", async (req, res) => {
  const movies = await Movies.find({}).populate("genure");
  res.json(movies);
});

// API to get details of a particular movie
app.get("/api/v1/movies/:id", async (req, res) => {
  const movies = await Movies.findById(req.params.id).populate("genure");
  res.json(movies);
});

// API to get list of all movies of given genure. You need to post genure in body of the post call (type:application/json) with field name "genure"
app.post("/api/v1/movies/genure", async (req, res) => {
  const movies = await Movies.find({
    genure: { $all: req.body.genure },
  }).populate("genure");
  res.json(movies);
});

//API to save the movie details
app.post("/api/v1/movies", validatorPost, async (req, res) => {
  const movie = new Movies({
    movie_name: req.body.movie_name,
    description: req.body.description,
    release_date: req.body.release_date,
    genure: req.body.genure,
    duration: req.body.duration,
    rating: req.body.rating,
    status: true,
  });
  const savedMovie = await movie.save();
  res.json(savedMovie);
});

//API to delete genures
app.post("/api/v1/movies/delete", validatorPost, async (req, res) => {
  const savedMovie = await Movies.update(
    {},
    { $pull: { genure: { $in: [req.body.genure] } } },
    { multi: true }
  );
  res.json(savedMovie);
});

//API to update the movie details
app.put("/api/v1/movies/:id", async (req, res) => {
  const savedMovie = await Movies.findOneAndUpdate(
    { _id: req.params.id },
    {
      $set: {
        movie_name: req.body.movie_name,
        description: req.body.description,
        release_date: req.body.release_date,
        genure: req.body.genure,
        duration: req.body.duration,
        rating: req.body.rating,
        status: req.body.status,
      },
    }
  );
  res.json(savedMovie);
});

//API to delete the movie
app.delete("/api/v1/movies/:id", async (req, res) => {
  const savedMovie = await Movies.findOneAndDelete({ _id: req.params.id });
  res.json(savedMovie);
});

module.exports = app;
