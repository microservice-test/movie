const mongoose = require("mongoose");

const GenureSchema = mongoose.Schema(
  {
    genure_name: {
      type: String,
      required: true,
    },
    description: { type: String, required: false },
    status: { type: Boolean, required: true, default: true },
  },
  { timestamps: true }
);

var Genures = (module.exports = mongoose.model("genure", GenureSchema));
