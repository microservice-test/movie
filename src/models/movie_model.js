let genure = require("./genure_model");
const mongoose = require("mongoose");

const MovieSchema = mongoose.Schema(
  {
    movie_name: {
      type: String,
      required: true,
    },
    description: { type: String, required: false },
    release_date: { type: Date, required: true },
    genure: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "genure",
        required: false,
      },
    ],
    duration: { type: String, required: true },
    rating: { type: Number, required: false },
    status: { type: Boolean, required: true, default: true },
  },
  { timestamps: true }
);

module.exports = mongoose.model("movie", MovieSchema);
