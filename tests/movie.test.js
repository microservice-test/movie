const mongoose = require("mongoose");
const MovieModel = require("../src/models/movie_model");
const MovieData = {
  movie_name: "Movie Test",
  description: "Movie Description",
  release_date: new Date(),
  duration: "3:30",
  genure: [],
};

describe("Movie Schema Test", () => {
  beforeAll(async () => {
    await mongoose.connect(
      global.__MONGO_URI__,
      { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true },
      (err) => {
        if (err) {
          console.error(err);
          process.exit(1);
        }
      }
    );
  });

  afterAll((done) => {
    // Closing the DB connection allows Jest to exit successfully.
    mongoose.connection.close();
    done();
  });

  it("create & save Movie successfully", async () => {
    const validMovie = new MovieModel(MovieData);
    const savedMovie = await validMovie.save();
    // Object Id should be defined when successfully saved to MongoDB.
    expect(savedMovie._id).toBeDefined();
    expect(savedMovie.movie_name).toBe(MovieData.movie_name);
    expect(savedMovie.description).toBe(MovieData.description);
    expect(savedMovie.release_date).toBe(MovieData.release_date);
    expect(savedMovie.duration).toBe(MovieData.duration);
    expect(savedMovie.status).toBe(true);
  });

  // You shouldn't be able to add in any field that isn't defined in the schema
  it("insert movie successfully, but the field does not defined in schema should be undefined", async () => {
    const MovieWithInvalidField = new MovieModel({
      movie_name: "Movie Test",
      release_date: new Date(),
      duration: "3:30",
    });
    const savedMovieWithInvalidField = await MovieWithInvalidField.save();
    expect(savedMovieWithInvalidField._id).toBeDefined();
    expect(savedMovieWithInvalidField.description).toBeUndefined();
  });

  // It should give error on the movie_name field
  it("create movie without required field should failed", async () => {
    const MovieWithoutRequiredField = new MovieModel({});
    let err;
    try {
      const savedMovieWithoutRequiredField = await MovieWithoutRequiredField.save();
      error = savedMovieWithoutRequiredField;
    } catch (error) {
      err = error;
    }
    expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
    expect(err.errors.movie_name).toBeDefined();
    expect(err.errors.release_date).toBeDefined();
    expect(err.errors.duration).toBeDefined();
  });

  // It should us told us the errors in on gender field.
  it("get method should give data", async () => {
    const savedMovieWithoutRequiredField = await MovieModel.findOne();
    expect(savedMovieWithoutRequiredField._id).toBeDefined();
    expect(savedMovieWithoutRequiredField.movie_name).toBeDefined();
  });
});
