const mongoose = require("mongoose");
const MovieModel = require("../src/models/movie_model");
const MovieData = {
  movie_name: "Movie Test",
  description: "Movie Description",
  release_date: new Date(),
  duration: "3:30",
  genure: [],
};
const MovieDataUpdate = {
  movie_name: "Movie Test Update",
  description: "Movie Description Update",
  release_date: new Date(),
  duration: "4:40",
  genure: [],
};
const request = require("supertest");
const app = require("../src/app");

describe("Movie API TEST", () => {
  let idSaved = "";
  beforeAll(async () => {
    await mongoose.connect(
      global.__MONGO_URI__,
      { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true },
      (err) => {
        if (err) {
          console.error(err);
          process.exit(1);
        }
      }
    );
  });

  afterAll((done) => {
    // Closing the DB connection allows Jest to exit successfully.
    mongoose.connection.close();
    done();
  });

  it("should check post request", async () => {
    const res = await request(app).post("/api/v1/movies").send(MovieData);
    idSaved = res.body._id;
    //console.log(res.body);
    expect(res.status).toBe(200);
    expect(res.body.movie_name).toBe(MovieData.movie_name);
    expect(res.body.description).toBe(MovieData.description);
    expect(res.body.duration).toBe(MovieData.duration);
    expect(res.body.rating).toBe(MovieData.rating);
    expect(res.body.status).toBe(true);
  });

  it("should check get request for all records", async () => {
    const res = await request(app).get("/api/v1/movies");
    expect(res.status).toBe(200);
  });

  it("should check get request for specific record", async () => {
    //console.log(idSaved);
    const res = await request(app).get("/api/v1/movies/" + idSaved);
    expect(res.status).toBe(200);
    expect(res.body._id).toBe(idSaved);
  });

  it("should check put request to update specific record", async () => {
    const res = await request(app)
      .put("/api/v1/movies/" + idSaved)
      .send(MovieDataUpdate);
    expect(res.status).toBe(200);
    expect(res.body._id).toBe(idSaved);
  });

  it("should check delete request for specific record", async () => {
    const res = await request(app).delete("/api/v1/movies/" + idSaved);
    expect(res.status).toBe(200);
  });
});
